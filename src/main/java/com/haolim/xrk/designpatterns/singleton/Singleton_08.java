package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  22:39
 * @Description: TODO
 * @Version: 1.0
 *
 * 不仅可以解决线程问题，还可以防止反序列化
 */
public enum Singleton_08 {

    INSTANCE;

    public void getIntance() {
    }

}
