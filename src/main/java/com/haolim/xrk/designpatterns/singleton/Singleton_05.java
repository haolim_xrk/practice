package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  21:53
 * @Description: TODO
 * @Version: 1.0
 * <p>
 * lazy loading
 * 也称懒汉模式
 * 虽然达到了按需初始化的目的，但也存在线程安全问题
 * 可以通过synchronized解决，效率也还是有问题
 */
public class Singleton_05 {

    // 设置私有的实例变量
    private static Singleton_05 INSTANCE;

    // 构造方法私有
    private Singleton_05() {

    }

    // 提供一个公共的获取实例的方法
    public static Singleton_05 getInstance() {
        if (INSTANCE == null) {
            synchronized (Singleton_05.class) {
                INSTANCE = new Singleton_05();
            }
        }
        return INSTANCE;
    }

}
