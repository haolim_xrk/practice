package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  22:25
 * @Description: TODO
 * @Version: 1.0
 * <p>
 * lazy loading
 * 也称懒汉式
 * 做了双重判断
 */
public class Singleton_06 {

    // 私有的实例变量
    private static Singleton_06 INSTANCE;

    // 私有的构造方法
    private Singleton_06() {

    }

    // 公共的提供实例方法
    public static Singleton_06 getInstance() {
        if (INSTANCE == null) {
            // 双重检查
            synchronized (Singleton_06.class) {
                if (INSTANCE == null) {
                    INSTANCE = new Singleton_06();
                }
            }
        }
        return INSTANCE;
    }
}
