package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  20:26
 * @Description: TODO 单例模式设计模式练习
 * @Version: 1.0
 * <p>
 * 思想第一步 把构造方法设置成私有的（保证只有这个类能拿到实例）
 */
public class Singleton_01 {

    private static final Singleton_01 INSTANCE = new Singleton_01();

    private Singleton_01() {
    }

    public static Singleton_01 getInstance() {
        return INSTANCE;
    }


}
