package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  20:42
 * @Description: TODO
 * @Version: 1.0
 * lazy loading
 * 即懒汉模式
 * 虽然达到了按需初始化的目的，但却带来了线程安全问题
 */
public class Singleton_03 {

    private static Singleton_03 INSTANCE;

    public static Singleton_03 Singleton_03() {
        if (INSTANCE == null) {
            INSTANCE = new Singleton_03();
        }
        return INSTANCE;
    }
}
