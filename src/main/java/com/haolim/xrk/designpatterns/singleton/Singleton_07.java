package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  22:31
 * @Description: TODO
 * @Version: 1.0
 * <p>
 * 静态内部类的方式
 * JVM保证单例
 * 加载外部类的时候不会加载内部类，这样可以实现单例懒加载
 */
public class Singleton_07 {
    private Singleton_07() {
    }

    private static class Singleton_07_Holder {
        private final static Singleton_07 INSTANCE = new Singleton_07();
    }

    public static Singleton_07 getInstance() {
        return Singleton_07_Holder.INSTANCE;
    }
}
