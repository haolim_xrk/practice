package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  20:46
 * @Description: TODO
 * @Version: 1.0
 */
public class Singleton_04 {

    // 定义私有的实例变量
    private static Singleton_04 INSTANCE;

    // 把构造方法设为私有的
    private Singleton_04() {

    }

    // 对外提供一个获取实例的公共方法
    public static synchronized Singleton_04 getInstance() {
        if (INSTANCE == null) {
            new Singleton_04();
        }
        return INSTANCE;
    }
}
