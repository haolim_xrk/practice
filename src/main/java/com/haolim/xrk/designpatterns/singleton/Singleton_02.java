package com.haolim.xrk.designpatterns.singleton;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  20:40
 * @Description: TODO
 * @Version: 1.0
 * 跟01是一个类型的
 */
public class Singleton_02 {

    private static final Singleton_02 INSTANCE;

    static {
        INSTANCE = new Singleton_02();
    }

    private Singleton_02() {
    }

    public static Singleton_02 getInstance() {
        return INSTANCE;
    }

}
