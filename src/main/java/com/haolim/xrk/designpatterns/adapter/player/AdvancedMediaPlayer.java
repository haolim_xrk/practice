package com.haolim.xrk.designpatterns.adapter.player;

/**
 * @author haolim
 * @version v1.0
 * @className AdvancedMediaPlayer
 * @date 2024年 01月 11日
 * @description TODO
 */
public interface AdvancedMediaPlayer {
    public void playVlc(String fileName);
    public void playMp4(String fileName);
}
