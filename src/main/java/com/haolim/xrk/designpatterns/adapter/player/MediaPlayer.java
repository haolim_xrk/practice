package com.haolim.xrk.designpatterns.adapter.player;

/**
 * @author haolim
 * @version v1.0
 * @className MediaPlayer
 * @date 2024年 01月 11日
 * @description TODO 播放的接口
 */
public interface MediaPlayer {
  
    /**
     * @Description
     * @Param audioType
     * @Param fileName
     * @Return void
     * @Author haolim
     * @Date 2024/1/11 0011
     **/
    public void play(String audioType, String fileName);
}
