package com.haolim.xrk.designpatterns.adapter.player;

/**
 * @author haolim
 * @version v1.0
 * @className VlcPlayer
 * @date 2024年 01月 11日
 * @description TODO
 */
public class VlcPlayer implements AdvancedMediaPlayer{
    @Override
    public void playVlc(String fileName) {
        System.out.println("Playing vlc file. Name: "+ fileName);
    }

    @Override
    public void playMp4(String fileName) {
        //什么也不做
    }
}
