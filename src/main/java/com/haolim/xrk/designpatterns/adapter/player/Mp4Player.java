package com.haolim.xrk.designpatterns.adapter.player;

/**
 * @author haolim
 * @version v1.0
 * @className Mp4Player
 * @date 2024年 01月 11日
 * @description TODO
 */
public class Mp4Player implements AdvancedMediaPlayer{
    @Override
    public void playVlc(String fileName) {
        // 什么也不做
    }

    @Override
    public void playMp4(String fileName) {
        System.out.println("Playing mp4 file. Name: "+ fileName);
    }
}
