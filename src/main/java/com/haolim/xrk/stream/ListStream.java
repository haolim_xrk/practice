package com.haolim.xrk.stream;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author: haolim
 * @CreateTime: 2024-01-09  21:55
 * @Description: TODO
 * @Version: 1.0
 */
public class ListStream {
    public ListStream() throws IOException {
        String context = Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get("application.txt")));

        List<String> words = Arrays.asList(context.split("\\PL+"));

        long count = 0;
        for (String word : words) {
            if (word.length() > 12) {
                count++;
            }
        }

        count = words.stream().filter(word -> word.length() > 12).count();

        count = words.parallelStream().filter(word -> word.length() > 12).count();

        final int SIZE = 10;

    }

    public static <T> void show(String title, Stream<T> stream) {
        final int SIZE = 10;
        List<T> fistElements = stream.limit(SIZE + 1).collect(Collectors.toList());
        System.out.println(title + ":");
    }
}
