package com.haolim.xrk;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HaolimXrkApplication {

    public static void main(String[] args) {
        SpringApplication.run(HaolimXrkApplication.class, args);
    }

}
